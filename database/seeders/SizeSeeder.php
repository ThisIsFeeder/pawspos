<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            'name' => 'តូច',
            'description' => 'Small',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('sizes')->insert([
            'name' => 'កណ្ដាល',
            'description' => 'Medium',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('sizes')->insert([
            'name' => 'ធំ',
            'description' => 'Large',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
