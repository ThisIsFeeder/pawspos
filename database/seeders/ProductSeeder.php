<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Hot
        DB::table('products')->insert([
            'code' => 'P0001',
            'name' => 'Hot Latte',
            'price' => 1.1,
            'image_path' => '',
            'category_id' => 1,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0002',
            'name' => 'Hot Moka',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 1,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0003',
            'name' => 'Hot កាពូស៊ីណូ',
            'price' => 1,
            'image_path' => '',
            'category_id' => 1,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0004',
            'name' => 'Hot អេចស្ពេសូ',
            'price' => 1.1,
            'image_path' => '',
            'category_id' => 1,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // Ice
        DB::table('products')->insert([
            'code' => 'P0005',
            'name' => 'Ice Coffee',
            'price' => 1,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0006',
            'name' => 'Ice Coffee Milk',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0007',
            'name' => 'Ice Latte',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0008',
            'name' => 'Ice ខារ៉ាមេ',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0009',
            'name' => 'Ice Moka',
            'price' => 1.6,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0010',
            'name' => 'Ice អាមេរិចខាណូរ',
            'price' => 1.1,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0011',
            'name' => 'ផាសិនសូដា',
            'price' => 0.65,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0012',
            'name' => 'ផាសិនទឹកដោះគោ',
            'price' => 0.75,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0013',
            'name' => 'តែជ្រក់',
            'price' => 0.65,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0014',
            'name' => 'កាហ្វេដូង',
            'price' => 1.1,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0015',
            'name' => 'អូវាន់ទីនដូង',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0016',
            'name' => 'សូកូឡាដូង',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0017',
            'name' => 'តែបែតង',
            'price' => 0.75,
            'image_path' => '',
            'category_id' => 2,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // Frappe & Smoothie
        DB::table('products')->insert([
            'code' => 'P0018',
            'name' => 'ប័រក្រឡុក',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0019',
            'name' => 'ទៀបក្រឡុក',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0020',
            'name' => 'ផាសិនក្រឡុង',
            'price' => 1,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0021',
            'name' => 'ស្វាយក្រឡុង',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0022',
            'name' => 'ចេកក្រឡុង',
            'price' => 1.25,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0023',
            'name' => 'តែបៃតងក្រឡុក',
            'price' => 1,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0024',
            'name' => 'សូកូឡាក្រឡុង',
            'price' => 1.1,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0025',
            'name' => 'ផាសិន+ស្វាយក្រឡុក',
            'price' => 1,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0026',
            'name' => 'កាហ្វេក្រឡុង',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0027',
            'name' => 'Mocaក្រឡុង',
            'price' => 1.75,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0028',
            'name' => 'ប៊្លូបឺរីក្រឡុង',
            'price' => 1.75,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0029',
            'name' => 'ស្ពរបឺរីក្រឡុង',
            'price' => 1.75,
            'image_path' => '',
            'category_id' => 4,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // Juice
        DB::table('products')->insert([
            'code' => 'P0030',
            'name' => 'ប៉មក្រហម',
            'price' => 1.75,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0031',
            'name' => 'ប៉មខៀវ',
            'price' => 1.75,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0032',
            'name' => 'ការ៉ូត',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0033',
            'name' => 'ម្នាស់',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0034',
            'name' => 'ក្រូចពោធិ៍សាត់',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0035',
            'name' => 'ស្រកានាគ',
            'price' => 1.5,
            'image_path' => '',
            'category_id' => 3,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // Snack
        DB::table('products')->insert([
            'code' => 'P0036',
            'name' => 'នំច្រូសង់',
            'price' => 0.75,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0037',
            'name' => 'នំច្រូសង់ស្នូលហតដក',
            'price' => 0.85,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0038',
            'name' => 'នំច្រូសង់ស្នូលសូកូឡា',
            'price' => 0.85,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0039',
            'name' => 'នំ Banana Cake',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0040',
            'name' => 'នំបុ័ងតូច',
            'price' => 0.15,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0041',
            'name' => 'នំបុ័ងបាយ៉ុង',
            'price' => 0.4,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0042',
            'name' => 'សាំងវ៉ិច',
            'price' => 0.75,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0043',
            'name' => 'នំបុ័ងហត',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0044',
            'name' => 'នំបុ័ងជ្រុង',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0045',
            'name' => 'នំបុ័ងហតដកសាប',
            'price' => 0.75,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0046',
            'name' => 'នំបុ័ងស្នូលហតដក',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0047',
            'name' => 'នំបុ័ងស្នូលសូកូឡា',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('products')->insert([
            'code' => 'P0048',
            'name' => 'នំបុ័ងសាច់',
            'price' => 1,
            'image_path' => '',
            'category_id' => 5,
            'description' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
