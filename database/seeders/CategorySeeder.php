<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'ក្ដៅ',
            'description' => 'hot drink',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('categories')->insert([
            'name' => 'ត្រជាក់',
            'description' => 'ice drink',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('categories')->insert([
            'name' => 'ទឹកផ្លែឈឺគៀបស្រស់',
            'description' => 'juices drink',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('categories')->insert([
            'name' => 'ក្រឡុង',
            'description' => 'frappes and smoothies drink',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('categories')->insert([
            'name' => 'នំ',
            'description' => 'foods',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
