@extends('master')

@section('content-header')
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<div class="container-fluid">
		  <div class="row mb-2">
			<div class="col-sm-6">
			  <h1>{{__('Size')}}</h1>
			</div>
			<div class="col-sm-6">
			  <ol class="breadcrumb float-sm-right">
				<li class="breadcrumb-item"><a href="{{route('home')}}">{{__('Home')}}</a></li>
				<li class="breadcrumb-item active">{{__('Size')}}</li>
			  </ol>
			</div>
		  </div>
		</div><!-- /.container-fluid -->
	  </section>
@endsection

@section('content')
	@yield('content')
@endsection

@push('scripts')
	<script>
		$(function () {
		  $("#example1").DataTable({
			"responsive": true,
			"autoWidth": false,
			"language": {
				"emptyTable": "No Size available in database"//Change your default empty table message
			}
		  });
		});
	  </script>
@endpush