@extends('size.layout')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
				<div class="card">
					<div class="card-header">
                        <a class="btn btn-primary float-left" href="{{ route('size.create') }}"><i class="fas fa-plus-square"> {{__('New')}}</i></a>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="example1" class="table table-bordered table-striped">
							<thead>
								<tr role="row">
									<th>{{__('Name')}}</th>
									<th>{{__('Description')}}</th>
									<th>{{__('Action')}}</th>
								</tr>
							</thead>
							<tbody>
							@foreach($sizes as $s)
								<tr>
									<td>{{$s->name}}</td>
									<td>{{$s->description}}</td>
									<td>
										<form action="{{ route('size.destroy',$s->id) }}" method="POST">
											<a class="btn btn-info" href="{{ route('size.edit', $s->id) }}"><i class="fas fa-edit"></i></a>
											@csrf
											@method('DELETE')
											<button class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fas fa-trash-alt"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>{{__('Name')}}</th>
									<th>{{__('Description')}}</th>
									<th>{{__('Action')}}</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection