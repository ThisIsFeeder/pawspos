@extends('report.layout')
@section('content')
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
	  <div class="row">
		<div class="col-12">
		  <div class="card">
			<div class="card-header">
				<form action="{{ route('report_product') }}">
					<div class="input-group date" id="reservationdate" data-target-input="nearest">
						<input id="input-date" name="order_date" type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>
						<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
						</div>
						<div class="input-group-append">
							<button class="btn btn-primary float-right"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
			  <table id="report_table" class="table table-bordered table-striped">
				<thead>
				<tr>
					<th>{{__('Product')}}</th>
					<th>{{__('Quantity')}}</th>
					<th>{{__('Price')}}</th>
					<th>{{__('Total Price')}}</th>
				</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
						<tr>
							<td>{{$order->name}}</td>
							<td>{{$order->qty}}</td>
							<td style="text-align: right">$ {{$order->price}}</td>
							<td style="text-align: right">$ {{$order->total}}</td>
						</tr>
					@endforeach
					<tr>
						<td>Total</td>
						<td>
							{{ 
						$orders->sum(function ($order) {
							return $order->qty;
						})
						}}
						</td>
						<td></td>
						<td style="text-align: right">
							$ {{ 
							$orders->sum(function ($order) {
								return $order->total;
							})
							}}
						</td>
					</tr>
					</tbody>
				<tfoot>
				<tr>
					<th>{{__('Product')}}</th>
					<th>{{__('Quantity')}}</th>
					<th>{{__('Price')}}</th>
					<th>{{__('Total Price')}}</th>
				</tr>
				</tfoot>
			  </table>
			</div>
			<!-- /.card-body -->
		  </div>
		  <!-- /.card -->
		</div>
		<!-- /.col -->
	  </div>
	  <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
  </section>
<!-- /.content -->
@endsection

@push('script')
<script>
	$(document).ready(() => {
		$('#input-date').change(() => {

		});
	})
</script>
@endpush