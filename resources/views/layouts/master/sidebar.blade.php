<aside class="main-sidebar elevation-4 sidebar-dark-purple">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link navbar-purple">
      <img src="{{url('/dashboard/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{__('Stock Management')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('/dashboard/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-legacy nav-flat" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link {{ Request::segment(1) === 'home' ? 'active' : null }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('Dashboard')}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('product.index') }}" class="nav-link {{ Request::segment(1) === 'product' ? 'active' : null }}">
              <i class="nav-icon fab fa-product-hunt"></i>
              <p>
                {{__('Product')}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('category.index') }}" class="nav-link {{ Request::segment(1) === 'category' ? 'active' : null }}">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                {{__('Category')}}
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('size.index') }}" class="nav-link {{ Request::segment(1) === 'size' ? 'active' : null }}">
              <i class="nav-icon fas fa-list-alt"></i>
              <p>
                {{__('Size')}}
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link {{ Request::segment(1) === 'report' ? 'active' : null}}">
              <i class="nav-icon fas fa-file"></i>
              <p>
                {{__('Report')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('report_product') }}" class="nav-link {{ Request::segment(2) === 'product' ? 'active' : null }}">
                  <i class="nav-icon fas fa-receipt"></i>
                  <p>
                    {{__('Product')}}
                  </p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link {{ Request::segment(1) === 'language' ? 'active' : null}} {{Request::segment(1) === 'branch' ? 'active' : null}}">
              <i class="nav-icon fas fa-toolbox"></i>
              <p>
                {{__('System')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/branch" class="nav-link {{ Request::segment(1) === 'branch' ? 'active' : null }}">
                  <i class="nav-icon fas fa-code-branch"></i>
                  <p>
                    {{__('Branch')}}
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/language" class="nav-link {{ Request::segment(1) === 'language' ? 'active' : null }}">
                  <i class="fas fa-language nav-icon"></i>
                  <p>{{__('Language')}}</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>