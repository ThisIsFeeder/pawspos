@extends('category.layout')
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{__('Edit Category')}}</h3>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" enctype="multipart/form-data" action="{{route('category.update', $category->id)}}" method="post">
                        {{method_field('put')}}
                        {{csrf_field()}}
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                            <input type="text" class="form-control name" name="name" value="{{$category->name}}" placeholder={{__('Name')}}>
                            </div>
                            <div class="form-group">
                                <label for="description">{{__('Description')}}</label>
                                <input type="text" class="form-control editdescription" name="description" value="{{$category->description}}" placeholder={{__('Description')}}>
                            </div>
                            {{-- <div class="form-group">
                                <div class="form-group">
                                    <label for="parent">{{__('Parent')}}</label>
                                    <div class="select2-info">
                                        <select class="select2 selectParentCategory" name="parentId[]" multiple="multiple" data-placeholder="{{__('Select Parents')}}" style="width: 100%;">
                                            @foreach($categories as $data)
                                                @if(in_array($data->id, $selectedParent))
                                                <option value="{{ $data->id }}" selected="true">{{ $data->name }}</option>
                                                @else
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endif 
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-default float-left" onclick="{{ route('category.index') }}">{{__('Cancel')}}</button>
                            <button type="submit" class="btn btn-info float-right">{{__('Edit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection