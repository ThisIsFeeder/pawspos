@extends('product.layout')
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">{{__('Edit Product')}}</h3>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" enctype="multipart/form-data" action="{{route('product.update', $product->id)}}" method="post">
                        {{method_field('PUT')}}
                        {{csrf_field()}}
                        <div class="card-body">
                            <div class="text-center">
                                <img id="image_preview_container" class="profile-user-img img-fluid img-thumbnail" style="width:100px; height: 100px;" src="/images/{{$product->image_path}}" alt="User profile picture">
                                <input type="file" name="image_path" value="{{$product->image_path}}" id="imgprofile" class="form-control d-none">
                            </div>
							<div class="text-center" id="edit">
								<label for="name">{{__('Choose')}}</label>
							</div>
                            <div class="form-group">
                                <label for="code">{{__('Code')}}</label>
                                <input type="text" class="form-control" name="code" value="{{$product->code}}" placeholder={{__("Code")}}>
                            </div>
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="name" value="{{$product->name}}" placeholder={{__("Name")}}>
                            </div>
                            <div class="form-group">
                                <label for="category">{{__('Category')}}</label>
                                <select class="form-control select2bs4" name="category_id" style="width: 100%;">
                                    @foreach($categories as $category)
                                        @if($category->id==$product->category_id)
                                            <option value="{{ $category->id }}" selected="true">{{ $category->name }}</option>
                                        @else
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="size">{{__('Size')}}</label>
                                <select class="form-control select2bs4" name="size_id" style="width: 100%;">
                                    @foreach($sizes as $size)
                                        @if($size->id==$product->size_id)
                                            <option value="{{ $size->id }}" selected="true">{{ $size->name }}</option>
                                        @else
                                            <option value="{{ $size->id }}">{{ $size->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price">{{__('Price')}}</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="price" value="{{$product->price}}" placeholder={{__("Price")}}>
                                    <div class="input-group-append">
                                        <span class="input-group-text">$</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">{{__('Description')}}</label>
                                <input type="text" class="form-control editdescription" name="description" value="{{$product->description}}" placeholder={{__("Description")}}>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-default float-left" href="{{ route('product.index') }}">{{__('Cancel')}}</a>
                            <button type="submit" class="btn btn-info float-right">{{__('Edit')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
	$(document).ready(() => {
        $('#edit').click(() => {
            $('#imgprofile').click();
        });
        $('#imgprofile').change(function(){
            let reader = new FileReader();
       
            reader.onload = (e) => { 
        
                $('#image_preview_container').attr('src', e.target.result); 
            }
       
            reader.readAsDataURL(this.files[0]); 
         
        });
    })
</script>
@endpush
