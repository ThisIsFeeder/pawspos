@extends('product.layout')
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">{{__('Create New Product')}}</h3>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" enctype="multipart/form-data" action="{{route('product.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="card-body box-profile">
			                <div class="text-center">
                                <img id="image_preview_container" class="profile-user-img img-fluid img-thumbnail" style="width:100px; height: 100px;" src="/dashboard/dist/img/boxed-bg.png" alt="User profile picture">
                                <input type="file" name="image_path" id="imgprofile" class="form-control d-none">
                            </div>
							<div class="text-center" id="edit">
								<label for="name">{{__('Choose')}}</label>
							</div>
                            <div class="form-group">
                                <label for="code">{{__('Code')}}</label>
                                <input type="text" class="form-control" name="code" value="" placeholder={{__("Code")}}>
                                {{-- <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-code"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="code" value="" placeholder={{__("Code")}}>
                                </div> --}}
                            </div>
                            <div class="form-group">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="name" value="" placeholder={{__("Name")}}>
                                {{-- <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-code"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="name" value="" placeholder={{__("Name")}}>
                                </div> --}}
                            </div>
                            <div class="form-group">
                                <label for="category">{{__('Category')}}</label>
                                <select name="category_id" class="form-control select2bs4" style="width: 100%;">
                                    @foreach($categories as $data)
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="category">{{__('Size')}}</label>
                                <select name="size_id" class="form-control select2bs4" style="width: 100%;">
                                    @foreach($sizes as $data)
                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price">{{__('Price')}}</label>
                                <div class="input-group">
                                    {{-- <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div> --}}
                                    <input type="text" class="form-control" name="price" value="" placeholder={{__("Price")}}>
                                    <div class="input-group-append">
                                        <span class="input-group-text">$</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description">{{__('Description')}}</label>
                                <input type="text" class="form-control editdescription" name="description" value="" placeholder={{__("Description")}}>
                            </div>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-default float-left" href="{{ route('product.index') }}">{{__('Cancel')}}</a>
                            <button type="submit" class="btn btn-primary float-right">{{__('Add')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script>
	$(document).ready(() => {
        $('#edit').click(() => {
            $('#imgprofile').click();
        });
        $('#imgprofile').change(function(){
            let reader = new FileReader();
       
            reader.onload = (e) => { 
        
                $('#image_preview_container').attr('src', e.target.result); 
            }
       
            reader.readAsDataURL(this.files[0]); 
         
        });
    })
</script>
@endpush
