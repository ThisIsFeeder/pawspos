<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function product(Request $request)
    {
        //
        $order_date = $request["order_date"];
        if(!$order_date){
            $order_date = today();
        }

        $orders = DB::table('orders')
                    ->join('order_details', 'order_details.order_id', '=', 'orders.id')
                    ->join('products', 'order_details.product_id', '=', 'products.id')
                    ->select(DB::raw('products.name as name, sum(order_details.quantity) as qty, products.price as price, sum(order_details.total_price) as total'))
                    ->whereDate('orders.created_at', '=', date('Y-m-d', strtotime($order_date)))
                    ->groupBy('order_details.product_id')
                    ->get();
        return view('report.product', compact('orders'));
    }

}
